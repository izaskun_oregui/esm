"""
On-line pattern scenario.
Cost function: DTW distance
"""

import numpy as np
import os
import sys
import matplotlib.pyplot as plt
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from OEM import OEM_online


X = np.array([0.0, 1.0, 2.0, 1.5, 1.5, 1.0, 2.0, 1.0, 1.0, 0.0])
Y = np.array([0.0, 0.0, 0.0, 1.0, 2.0, 1.5, 1.0, 2.0, 1.0, 0.0])
band_width = 5

scenario = OEM_online(w=0.1, l=band_width, dist='euclidean')
XY = scenario.init_dist(X,Y)

plt.imshow(XY, vmin=0, vmax=6)
plt.title('Recursive Matrix. Sakoe-Chiba l=%d' % band_width)
plt.xticks(np.arange(10)+0.5)
plt.yticks(np.arange(10)+0.5)
plt.xlim([-0.5,9.5])
plt.ylim([-0.5,9.5])
plt.grid(True, linestyle='--')
plt.colorbar()
plt.show()

for i in range(10):

	X_new = Y[i]
	Y_new = X[i]
	colval, rowval = scenario.update_dist(X_new,Y_new)
	col = np.concatenate((np.array([float('Inf')]*(XY.shape[1] + X_new.size - colval.size)),colval), axis=None)
	row = np.concatenate((np.array([float('Inf')]*(XY.shape[0] + Y_new.size - rowval.size)),rowval), axis=None)

	XY = np.vstack((XY, row[:-1]))
	XY = np.column_stack((XY, col))

	plt.imshow(XY, vmin=0, vmax=6)
	plt.title('Recursive Matrix. Sakoe-Chiba l=%d' % band_width)
	plt.xticks(np.arange(XY.shape[0])+0.5)
	plt.yticks(np.arange(XY.shape[1])+0.5)
	plt.xlim([0.5+i, 10.5+i])
	plt.ylim([0.5+i, 10.5+i])
	plt.grid(True, linestyle='--')
	plt.colorbar()
	plt.show()
