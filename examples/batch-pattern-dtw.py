"""
Batch pattern scenario.
Cost function: DTW distance
"""

import numpy as np
import os
import sys
import matplotlib.pyplot as plt
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from OEM import OEM_batch


X = np.array([0.0, 1.0, 2.0, 1.5, 1.5, 1.0, 2.0, 1.0, 1.0, 0.0])
Y = np.array([0.0, 0.0, 0.0, 1.0, 2.0, 1.5, 1.0, 2.0, 1.0, 0.0])
band_width = 5

scenario = OEM_batch(w=0.1, R=X, dist='euclidean')
XY = scenario.init_dist(Y)

plt.imshow(XY, vmin=0, vmax=6)
plt.title('Recursive Matrix')
plt.xticks(np.arange(10)+0.5)
plt.yticks(np.arange(10)+0.5)
plt.xlim([-0.5,9.5])
plt.ylim([-0.5,9.5])
plt.grid(True, linestyle='--')
plt.colorbar()
plt.show()

for i in range(10):

	Y_new = X[i]
	col = scenario.update_dist(Y_new)
	XY = np.column_stack((XY, col))
	print 0.5 + np.arange(XY.shape[1])

	plt.imshow(XY, vmin=0, vmax=6)
	plt.title('Recursive Matrix')
	plt.xticks(np.arange(XY.shape[1])+0.5)
	plt.yticks(np.arange(XY.shape[0])+0.5)
	plt.ylim([-0.5,9.5])
	plt.xlim([0.5+i, 10.5+i])
	plt.grid(True, linestyle='--')
	plt.colorbar()
	plt.show()
