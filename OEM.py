#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import warnings


class OEM_online:
	"""
	Online Elastic Similarity class

	Parameters
	----------
	l : int, optional, default: 10
		Sakoe-Chiba band width. Must be bigger than 1.
	w : float
		Online similarity measure memory. Must be between 0 and 1.
	rateX : float, optinoal, default: 1
		Reference Time series generation rate
	rateY : float, optinoal, default: None
		Query Time series generation rate. If rateY = None it takes rateX value
	dist : string, optional, default: 'euclidean'
		cost function
		OPTIONS:
		'euclidean': np.sqrt((x_i - y_j)**2)
		'edr':       1 if abs(x_i - y_j) >= epsilon else 0
		'edit':		 1 if x_i != y_j else 0
		'erp':       abs(x_i - y_j) if abs(x_i - y_j) >= epsilon else 0
	epsilon : float, optional, default: None
		edr threshold parameter
	"""

	def __init__(self, w, l=10, dist='euclidean', epsilon=None):

		self.dist = dist

		if l <= 0:
			warnings.warn('Sakoe-Chiba band has been set to l=1')
			self.l = 1
		else:
			self.l = l

		if (w >= 0) and (w <= 1):
			self.w = w
		else:
			raise ValueError('w must be between 0 and 1')

		if (epsilon is None) and (dist in ['edr', 'erp']):
			raise ValueError('for dist edr or erp epsilon must be a non negative float')
		elif (epsilon is not None) and epsilon < 0:
			raise ValueError('epsilon must be a non negative float')
		self.epsilon = epsilon

		self.m = 0
		self.n = 0

	def init_dist(self,R,S):
		"""
		Initial Similarity Measure

		Parameters
		----------
		R : 1-D array_like
			Array containing time series observations
		S : 2-D array_like
			Array containing time series observations

		Returns
		-------
		dist: float
			Elastic Similarity Measure between R (pattern time series) and S (query time series)
		"""

		if isinstance(S, (np.ndarray)) and R.size > 2:
			r = R.size
		else:
			raise ValueError('R time series must have more than 2 instances')

		if isinstance(S, (np.ndarray)) and S.size > 2:
			s = S.size
		else:
			raise ValueError('S time series must have more than 2 instances')

		# Compute point-wise distance
		if self.dist == 'euclidean':
			RS = self.__euclidean(R,S)
		elif self.dist == 'edr':
			RS = self.__edr(R, S, self.epsilon)
		elif self.dist == 'erp':
			RS = self.__erp(R, S, self.epsilon)
		elif self.dist == 'edit':
			RS = self.__edit(R, S)

		# compute recursive distance matrix using dynamic programing
		RS = self.__DP(RS, l=self.l, w=self.w)

		# Save the statistics, size O(self.l + self.l)
		if self.l >= r:
			self.R = R
			self.dtwR = RS[:,-1]
		else:
			self.R = R[-self.l:]
			self.dtwR = RS[-self.l:,-1]

		if self.l >= s:
			self.S = S
			self.dtwS = RS[-1,:]
		else:
			self.S = S[-self.l:]
			self.dtwS = RS[-1,-self.l:]

		# Save the starting index of the next chunk
		r += self.m
		s += self.n
		self.m = np.max([0,r-s])
		self.n = np.max([0,s-r])

		return RS

	def update_dist(self,X,Y):
		'''
		Add new observations to query time series

		Parameters
		----------
		X : array_like or float
			new reference time series observation(s)
		Y : array_like or float
			new query time series observation(s)

		Returns
		-------
		dist : float
			Updated distance

		Warning: the time series have to be non-empty
		(at least composed by a single measure)

		  -----------
		X | XS | XY |
		  -----------
		R | RS | RY |
		  -----------
		    S    Y
		'''

		if isinstance(X, (np.ndarray)):
			m = X.size
		else:
			m = 1
			X = np.array([X])

		if isinstance(Y, (np.ndarray)):
			n = Y.size
		else:
			n = 1
			Y = np.array([Y])

		# Solve XS
		dtwXSv, dtwXSh = self._solveXS(X,self.S,self.dtwS,iniI=self.m,iniJ=self.n-self.l)
		# Solve RY
		dtwRYh, dtwRYv = self._solveRY(self.R,Y,self.dtwR,iniI=self.m-self.l,iniJ=self.n)
		# Solve XY
		dtwXYh, dtwXYv = self._solveXY(X,Y,dtwXSv,self.dtwS[-1],dtwRYh,iniI=self.m,iniJ=self.n)

		# Save the statistics, size O(self.l + self.l)
		if m == 1 and n == 1:

			if len(self.R) < self.l:
				self.R = np.concatenate((self.R, X))
				self.dtwR = np.concatenate((self.dtwR,dtwXYv))
			else:
				self.R = np.concatenate((self.R[1:], X))
				self.dtwR = np.concatenate((self.dtwR[1:],dtwXYv))

			if len(self.S) < self.l:
				self.S = np.concatenate((self.S, Y))
				self.dtwS = np.concatenate((self.dtwS,dtwXYh))
			else:
				self.S = np.concatenate((self.S[1:], Y))
				self.dtwS = np.concatenate((self.dtwS[1:],dtwXYh))
		else:

			if(len(self.R) < self.l):
				self.R = np.concatenate((self.R[-(self.l-m):], X))
				self.dtwR = np.concatenate((dtwRYv[-(self.l-m):],dtwXYv))
			else:
				self.R = X[m-self.l:m]
				self.dtwR = dtwXYv[m-self.l:m]

			if(len(self.S) < self.l):
				self.S = np.concatenate((self.S[-(self.l-n):], Y))
				self.dtwS = np.concatenate((dtwXSh[-(self.l-n):],dtwXYh))
			else:
				self.S = Y[n-self.l:n]
				self.dtwS = dtwXYh[n-self.l:n]

		# Save the starting index of the next chunk
		m += self.m
		n += self.n
		self.m = np.max([0,m-n])
		self.n = np.max([0,n-m])

		return self.dtwR, self.dtwS

		# return min(np.concatenate((self.dtwR, self.dtwS)))

	def __euclidean(self,X,Y):
		Y_tmp, X_tmp = np.meshgrid(Y,X)
		XY = np.sqrt((X_tmp - Y_tmp)**2)
		return XY

	def __edr(self,X,Y, epsilon):
		Y_tmp, X_tmp = np.meshgrid(Y,X)
		XY = 1.0*(abs(X_tmp - Y_tmp) < epsilon)
		return XY

	def __erp(self,X,Y, epsilon):
		Y_tmp, X_tmp = np.meshgrid(Y,X)
		XY = abs(X_tmp - Y_tmp)
		XY[XY < epsilon] = 0
		return XY

	def __edit(self,X,Y):
		Y_tmp, X_tmp = np.meshgrid(Y,X)
		XY = 1.0*(X_tmp == Y_tmp)
		return XY

	def __DP(self, XY, l=1, w=1.0):

		m, n = np.shape(XY)

		# Solve first row
		for j in range(1,n):
			if j < l:
				XY[0,j] += (w if j <= 0 else 1)*XY[0,j-1]
			else:
				XY[0,j:] = float('Inf')
				break

		# Solve first column
		for i in range(1,m):
			if i < l:
				XY[i,0] += (w if i <= 0 else 1)*XY[i-1,0]
			else:
				XY[i:,0] = float('Inf')

		# Solve the rest
		for i in range(1,m):
			j_ini = np.max([1, i-l+1])
			j_end = np.min([n,i+l])
			XY[i, 1:j_ini] = float('Inf')
			XY[i,j_end:] = float('Inf')
			for j in range(j_ini,j_end):
				if j == j_ini and j == i-l+1:
					XY[i,j] += np.min([
						(w if (i <= j) else 1)*XY[i-1,j],
						w*XY[i-1,j-1]])
				elif j == j_end-1 and j == i+l-1:
					XY[i,j] += np.min([
						w*XY[i-1,j-1],
						(w if (j <= i-1) else 1)*XY[i,j-1]])
				else:
					XY[i,j] += np.min([
						(w if (i <= j) else 1)*XY[i-1,j],
						w*XY[i-1,j-1],
						(w if (j <= i-1) else 1)*XY[i,j-1]])
		return XY

	def _solveXS(self,X,S,dtwS,iniI=0,iniJ=0):
		'''
		X,S: to (partial) time series
		 -----------
		X| XS | XY |
		 -----------
		R|prev| RY |
		 -----------
		   S    Y

		dtwS: partial solutions to DTW(R,S)
		iniI: Index of the first point of X in the complete time series
		iniJ: Index of the first point of S in the complete time series

		* Warning *: X and S have to be nonempty (partial) series
		Falta: tener en cuenta rateX y rateY
		'''

		# Compute point-wise distance
		if self.dist == 'euclidean':
			XS = self.__euclidean(X,S)
		elif self.dist == 'edr':
			XS = self.__edr(X, S, self.epsilon)
		elif self.dist == 'erp':
			XS = self.__erp(X, S, self.epsilon)
		elif self.dist == 'edit':
			XS = self.__edit(X, S)

		m,s = X.size,S.size

		if self.dist == 'euclidean':

			# Solve first row
			j_ini = np.max([0,s-self.l+1])
			if j_ini == 0:
				XS[0,0] += (self.w if iniI <= iniJ else 1)*dtwS[0]
			else:
				XS[0,:j_ini] = float('Inf')
			for j in range(j_ini+1,s):
				XS[0,j] += np.min([
					(self.w if (iniJ+j) <= iniI else 1)*XS[0,j-1],
					self.w*dtwS[j-1],
					(self.w if iniI <= (iniJ+j) else 1)*dtwS[j]])

			# Solve first column
			for i in range(1,m):
					if s-self.l+1+i <= 0:
						XS[i,0] += XS[i-1,0]
					else:
						XS[i:,0] = float('Inf')
						break

			# Solve the rest
			for i in range(1,m):
				j_ini = np.max([1,s-self.l+1+i])
				for j in range(j_ini,s):
					if j_ini == 1 and (s-self.l+1+i) == 1:
						XS[i,j] += np.min([
							self.w*XS[i-1,j-1],
							(self.w if (iniJ+j <= iniI+i-1) else 1)*XS[i,j-1]])
					else:
						XS[i,1:j_ini] = float('Inf')
						XS[i,j] += np.min([
							(self.w if (iniI+i <= iniJ+j) else 1)*XS[i-1,j],
							self.w*XS[i-1,j-1],
							(self.w if (iniJ+j <= iniI+i-1) else 1)*XS[i,j-1]])
				if j_ini >= s:
					XS[i:,:] = float('Inf')
					break

		return XS[:,-1], XS[-1,:]

	def _solveRY(self,R,Y,dtwR,iniI=0,iniJ=0):
		'''
		R, Y: to (partial) time series
		 -----------
		X| XS | XY |
		 -----------
		R|prev| RY |
		 -----------
		   S    Y

		dtwR: partial solutions of DTW(R,S)
		iniI: Index of the first point of R in the complete time series
		iniJ: Index of the first point of Y in the complete time series

		* Warning *: R and Y have to be nonempty (partial) series
		'''

		# Compute point-wise distance
		if self.dist == 'euclidean':
			RY = self.__euclidean(R,Y)
		elif self.dist == 'edr':
			RY = self.__edr(R, Y, self.epsilon)
		elif self.dist == 'erp':
			RY = self.__erp(R, Y, self.epsilon)
		elif self.dist == 'edit':
			RY = self.__edit(R, Y)

		r,n = R.size,Y.size

		# First first column
		i_ini = np.max([0,r-self.l+1])
		if i_ini == 0:
			RY[0,0] += (self.w if iniJ <= iniI else 1)*dtwR[0]
		else:
			RY[:i_ini,0] = float('Inf')
		for i in range(i_ini+1,r):
			RY[i,0] += np.min([
				(self.w if (iniI+i) <= iniJ else 1)*RY[i-1,0],
				self.w*dtwR[i-1],
				(self.w if iniJ <= iniI+i else 1)*dtwR[i]])

		# Solve first row
		for j in range(1,n):
			if r-self.l+1+j <= 0:
				RY[0,j] += RY[0,j-1]
			else:
				RY[0,j:] = float('Inf')
				break

		# Solve the rest
		for j in range(1,n):
			i_ini = np.max([1,r-self.l+1+j])
			for i in range(i_ini,r):
				if i_ini == 1 and (r-self.l+1+j) == 1:
					RY[i,j] += np.min([
						self.w*RY[i-1,j-1],
						(self.w if (iniJ+j) <= (iniI+i) else 1)*RY[i,j-1]])
				else:
					RY[1:i_ini,j] = float('Inf')
					RY[i,j] += np.min([
						(self.w if (iniI+i) <= (iniJ+j) else 1)*RY[i-1,j],
						self.w*RY[i-1,j-1],
						(self.w if (iniJ+j) <= (iniI+i) else 1)*RY[i,j-1]])
			if i_ini >= r:
				RY[:,j:] = float('Inf')
				break

		return RY[-1,:], RY[:,-1]

	def _solveXY(self,X,Y,dtwXS,dtwRS,dtwRY,iniI=0,iniJ=0):
		'''
		X,Y: (partial) time series
		 -----------
		X| XS | XY |
		 -----------
		R| RS | RY |
		 -----------
		   S    Y

		dtwRS: solution to DTW(R,S)
		dtwXS: partial solutions to DTW(X,S), DTW(X,S)[:,-1]
		dtwRY: partial solutions to DTW(R,Y), DTW(R,Y)[-1,:]
		iniI: Index of the first point of X in the complete time series
		iniJ: Index of the first point of Y in the complete time series

		* Warning *: X and Y have to be nonempty (partial) series
		Falta: tener en cuenta rateX y rateY
		'''

		# Compute point-wise distance
		if self.dist == 'euclidean':
			XY = self.__euclidean(X,Y)
		elif self.dist == 'edr':
			XY = self.__edr(X, Y, self.epsilon)
		elif self.dist == 'erp':
			XY = self.__erp(X, Y, self.epsilon)
		elif self.dist == 'edit':
			XY = self.__edit(X, Y)

		m,n = X.size,Y.size

		# Solve the first point
		XY[0,0] += np.min([
			(self.w if iniJ <= iniI else 1)*dtwXS[0],
			self.w*dtwRS,
			(self.w if iniI <= iniJ else 1)*dtwRY[0]])

		# Solve first row
		for j in range(1,n):
			if j + 1 == self.l:
				XY[0,j] += np.min([
					(self.w if (iniJ+j) <= iniI else 1)*XY[0,j-1],
					self.w*dtwRY[j-1]])
			elif j < self.l:
			    XY[0,j] += np.min([
					(self.w if (iniJ+j) <= iniI else 1)*XY[0,j-1],
					self.w*dtwRY[j-1],
					(self.w if iniI <= (iniJ+j) else 1)*dtwRY[j]])
			else:
				XY[0,j:] = float('Inf')
				break

		# First first column
		for i in range(1,m):
			if i+1 < self.l:
				XY[i,0] += np.min([
					(self.w if (iniI+i) <= iniJ else 1)*XY[i-1,0],
					self.w*dtwXS[i-1]])
			elif i < self.l:
				XY[i,0] += np.min([
					(self.w if (iniI+i) <= iniJ else 1)*XY[i-1,0],
					self.w*dtwXS[i-1],
					(self.w if iniJ <= iniI+i else 1)*dtwXS[i]])
			else:
				XY[i:,0] = float('Inf')

		# Solve the rest
		for i in range(1,m):
			j_ini = np.max([1, i-self.l+1])
			j_end = np.min([n,i+self.l])
			for j in range(j_ini,j_end):
				if j == j_ini and j == (i-self.l+1):
					XY[i,j] += np.min([
						(self.w if (iniI+i) <= (iniJ+j) else 1)*XY[i-1,j],
						self.w*XY[i-1,j-1]])
				elif j == j_end-1 and j == i+self.l-1:
					XY[i,j] += np.min([
						self.w*XY[i-1,j-1],
						(self.w if (iniJ+j) <= (iniI+i) else 1)*XY[i,j-1]])
				else:
					XY[i,j] += np.min([
						(self.w if (iniI+i) <= (iniJ+j) else 1)*XY[i-1,j],
						self.w*XY[i-1,j-1],
						(self.w if (iniJ+j) <= (iniI+i) else 1)*XY[i,j-1]])
			XY[i,1:j_ini] = float('Inf')
			XY[i,j_end:] = float('Inf')

		return XY[-1,:], XY[:,-1]


class OEM_batch:
	"""
	Online Elastic Similarity class

	Parameters
	----------
	R : 1-D array_like
		time series pattern
	w : float
		Online similarity measure memory. Must be between 0 and 1.
	rateX : float, optinoal, default: 1
		Reference Time series generation rate
	rateY : float, optinoal, default: None
		Query Time series generation rate. If rateY = None it takes rateX value
	dist : string, optional, default: 'euclidean'
		cost function
		OPTIONS:
		'euclidean': np.sqrt((x_i - y_j)**2)
		'edr':       1 if abs(x_i - y_j) >= epsilon else 0
		'edit':		 1 if x_i != y_j else 0
		'erp':       abs(x_i - y_j) if abs(x_i - y_j) >= epsilon else 0
	epsilon : float, optional, default: None
		edr threshold parameter
	"""

	def __init__(self, R, w, dist='euclidean', epsilon=None):

		if dist in ['euclidean', 'edr', 'erp', 'edit']:
			self.dist = dist
		else:
			raise ValueError('Not valid dist value')

		if isinstance(R, (np.ndarray)) and R.size > 2:
			self.R = R

		if (w >= 0) and (w <= 1):
			self.w = w
		else:
			raise ValueError('w must be between 0 and 1')

		if (epsilon is None) and (dist in ['edr', 'erp']):
			raise ValueError('for dist edr or erp epsilon must be a non negative float')
		elif (epsilon is not None) and epsilon < 0:
			raise ValueError('epsilon must be a non negative float')
		self.epsilon = epsilon

	def init_dist(self,S):
		"""
		Initial Similarity Measure

		Parameters
		----------
		S : 1-D array_like
			Array containing time series observations

		Returns
		-------
		dist: float
			Elastic Similarity Measure between R (pattern time series) and S (query time series)
		"""

		if isinstance(S, (np.ndarray)) and S.size > 2:
			pass
		else:
			raise ValueError('S time series must have more than 2 instances')

		# Compute point-wise distance
		if self.dist == 'euclidean':
			RS = self.__euclidean(self.R,S)
		elif self.dist == 'edr':
			RS = self.__edr(self.R, S, self.epsilon)
		elif self.dist == 'erp':
			RS = self.__erp(self.R, S, self.epsilon)
		elif self.dist == 'edit':
			RS = self.__edit(self.R, S)

		# compute recursive distance matrix using dynamic programing
		r, s = np.shape(RS)

		# Solve first row
		for j in range(1,s):
			RS[0,j] += self.w*RS[0,j-1]

		# Solve first column
		for i in range(1,r):
			RS[i,0] += RS[i-1,0]

		# Solve the rest
		for i in range(1,r):
			for j in range(1,s):
				RS[i,j] += np.min([RS[i-1,j], self.w*RS[i-1,j-1], self.w*RS[i,j-1]])

		# save statistics
		self.dtwR = RS[:,-1]

		return RS

	def update_dist(self,Y):
		'''
		Add new observations to query time series

		Parameters
		----------
		Y : array_like or float
			new query time series observation(s)

		Returns
		-------
		dist : float
			Updated distance

		Warning: the time series have to be non-empty
		(at least composed by a single measure)


		  -----------
		R | RS | RY |
		  -----------
		    S    Y
		'''

		if isinstance(Y, (np.ndarray)):
			pass
		else:
			Y = np.array([Y])

		# Solve RY
		dtwRY = self._solveRY(Y,self.dtwR)

		# Save statistics
		self.dtwR = dtwRY

		return dtwRY

	def __euclidean(self,X,Y):
		Y_tmp, X_tmp = np.meshgrid(Y,X)
		XY = np.sqrt((X_tmp - Y_tmp)**2)
		return XY

	def __edr(self,X,Y, epsilon):
		Y_tmp, X_tmp = np.meshgrid(Y,X)
		XY = 1.0*(abs(X_tmp - Y_tmp) < epsilon)
		return XY

	def __erp(self,X,Y, epsilon):
		Y_tmp, X_tmp = np.meshgrid(Y,X)
		XY = abs(X_tmp - Y_tmp)
		XY[XY < epsilon] = 0
		return XY

	def __edit(self,X,Y):
		Y_tmp, X_tmp = np.meshgrid(Y,X)
		XY = 1.0*(X_tmp == Y_tmp)
		return XY

	def _solveRY(self,Y,dtwR):
		'''
		R, Y: to (partial) time series
		 -----------
		R|prev| RY |
		 -----------
		   S    Y

		dtwR: partial solutions of DTW(R,S)
		iniI: Index of the first point of R in the complete time series
		iniJ: Index of the first point of Y in the complete time series

		* Warning *: R and Y have to be nonempty (partial) series
		'''

		# Compute point-wise distance
		if self.dist == 'euclidean':
			RY = self.__euclidean(self.R,Y)
		elif self.dist == 'edr':
			RY = self.__edr(self.R, Y, self.epsilon)
		elif self.dist == 'erp':
			RY = self.__erp(self.R, Y, self.epsilon)
		elif self.dist == 'edit':
			RY = self.__edit(self.R, Y)

		r,n = np.shape(RY)

		print RY
		print dtwR

		# First first column
		RY[0,0] += self.w*dtwR[0]
		for i in range(1,r):
			RY[i,0] += np.min([
				RY[i-1,0], self.w*dtwR[i-1], self.w*dtwR[i]])

		# Solve first row
		for j in range(1,n):
			RY[0,j] += self.w*RY[0,j-1]

		# Solve the rest
		for j in range(1,n):
			for i in range(1,r):
				RY[i,j] += np.min([RY[i-1,j], self.w*RY[i-1,j-1], self.w*RY[i,j-1]])

		return RY[:,-1]
