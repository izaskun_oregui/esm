#On-line Elastic Similarity Measures for Time Series


Adaptation of the most frequantly used elastic similarity measures: *Dynamic Time
Warping (DTW), Edit Distance (Edit), Edit Distance for Real Sequences (EDR) and Edit Distance with Real Penalty (ERP)* to on-line setting. 

### OESM definition

Consider ![](https://latex.codecogs.com/svg.latex?%5Cinline%20X%5Em%3D%28x_1%2C...%2Cx_m%29) and ![](https://latex.codecogs.com/svg.latex?%5Cinline%20Y%5En%3D%28y_1%2C...%2Cy_n%29) time series. In the on-line setting, the weight of a path *p* is redefined to be

![](https://latex.codecogs.com/svg.latex?%5Cinline%20w_%5Crho%28p%29%3D%20%5Csum_%7B%28i%2Cj%29%20%5Cin%20p%7D%20%5Crho%5E%7Bf%28i%2Cj%29%7D%20c_%7Bi%2Cj%7D) 

where ![](https://latex.codecogs.com/svg.latex?%5Cinline%20f%3A%5Cmathbb%7BN%7D%5E2%5Crightarrow%5Cmathbb%7BR%7D) is the memory function, ![](https://latex.codecogs.com/svg.latex?%5Cinline%20c_%7Bi%2Cj%7D) is the *distance* (or cost function) between ![](https://latex.codecogs.com/svg.latex?%5Cinline%20x_i) and ![](https://latex.codecogs.com/svg.latex?%5Cinline%20y_j) samples, and ![](https://latex.codecogs.com/svg.latex?%5Cinline%20%5Crho%20%5Cin%20%280%2C1%5D) the memory parameter. In this context, the OESM  of the query time series ![](https://latex.codecogs.com/svg.latex?%5Cinline%20Y%5En) with respect to the reference pattern ![](https://latex.codecogs.com/svg.latex?%5Cinline%20X%5Em) is defined as:

![](https://latex.codecogs.com/svg.latex?%5Cinline%20D_%7B%5Crho%7D%28X%5Em%2CY%5En%29%3D%5Cmin_%7Bp%20%5Cin%20%5Cmathcal%7BP%7D%7D%20w_%7B%5Crho%7D%28p%29)


The code for the On-line Elastic Similarity Measures computation can be found at [OESM](code/). Two problem scenarios are considered: the batch pattern and the on-line pattern. The first illustrates the case where reference time series (batch patterns) are stored and only query sequences receive new examples. In this case the memroy function is defined as ![](https://latex.codecogs.com/svg.latex?%5Cinline%20f%28i%2Cj%29%3Dn-j). The second corresponds to the case where both the query and reference time series (on-line patterns) receive new data points. For the on-line pattern scheme, the meroy function is given by ![](https://latex.codecogs.com/svg.latex?%5Cinline%20f%28i%2Cj%29%3D%5Cmax%5C%7Bm-i%2Cn-j%5C%7D). A detailed description of the DTW adaptation (in the on-line pattern scenario) can be found in our [Pattern Recognition](https://www.sciencedirect.com/science/article/pii/S003132031830428X) or [ECML](https://link.springer.com/chapter/10.1007/978-3-319-71246-8_36) papers.

---

#### Supported cost functions

* DTW:  
    *   ![](https://latex.codecogs.com/svg.latex?c%28x_i%2Cy_j%29%3D%7C%7Cx_i-y_j%7C%7C_%7B2%7D)
* EDIT:  
    * ![](https://latex.codecogs.com/svg.latex?%5Cbegin%7Barray%7D%7Bccc%7D%20c%28x_i%2Cy_j%29%20%26%20%3D%20%26%20%5Cleft%5C%7B%20%5Cbegin%7Barray%7D%7Bll%7D%200%20%26%20%5Cmbox%7Bif%20%7D%20x_i%20%3D%20y_j%20%5C%5C%201%20%26%20%5Cmbox%7Botherwise%7D%20%5Cend%7Barray%7D%20%5Cright.%20%5Cend%7Barray%7D)
* EDR: Given ![](https://latex.codecogs.com/gif.latex?%5Cdelta%20%3E%200),
    * ![](https://latex.codecogs.com/svg.latex?%5Cinline%20%5Cbegin%7Barray%7D%7Bccc%7D%20c%28x_i%2Cy_j%3B%5Cdelta%29%20%26%20%3D%26%20%5Cleft%5C%7B%20%5Cbegin%7Barray%7D%7Bll%7D%200%20%26%20%5Cmbox%7Bif%20%7D%20%7Cx_i%20-%20y_j%7C%20%5Cleq%20%5Cdelta%20%5C%5C%201%20%26%20%5Cmbox%7Botherwise%7D%20%5Cend%7Barray%7D%20%5Cright.%20%5Cend%7Barray%7D)
* ERP: Given ![](https://latex.codecogs.com/gif.latex?%5Cdelta%20%3E%200),
    * ![](https://latex.codecogs.com/svg.latex?%5Cinline%20%5Cbegin%7Barray%7D%7Bccc%7D%20c%28x_i%2Cx_j%3B%5Cdelta%29%3D%5Cleft%5C%7B%20%5Cbegin%7Barray%7D%7Bll%7D%200%20%26%20%5Cmbox%7Bif%20%7D%20%7Cx_i%20-%20y_j%7C%20%5Cleq%20%5Cdelta%20%5C%5C%20%7Cx_i%20-%20y_j%7C%20%26%20%5Cmbox%7Botherwise%7D%20%5Cend%7Barray%7D%20%5Cright.%20%5Cend%7Barray%7D)

---

# Citation

```
@inproceedings{oregi2017,
  title={On-Line Dynamic Time Warping for Streaming Time Series},
  author={Oregi, Izaskun and Perez, Aritz and Del Ser, Javier and Lozano, Jose A},
  booktitle={Joint European Conference on Machine Learning and Knowledge Discovery in Databases},
  pages={591--605},
  year={2017},
  organization={Springer}
}
```

```
@article{oregi2019,
title = {On-line Elastic Similarity Measures for time series},
author = {Izaskun Oregi and Aritz Perez and Javier Del Ser and Jose A. Lozano},
journal = {Pattern Recognition},
volume = {88},
pages = {506 - 517},
year = {2019}
}
```